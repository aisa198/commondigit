﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommonDigit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;

namespace CommonDigit.Tests
{
    [TestClass()]
    public class ProgramTests
    {

        [TestMethod()]
        public void CountDigitApperanceInNumberTest_ValidData_ValidResult()
        {
            var programLoop = new ProgramLoop();
            var result1 = programLoop.CountDigitApperanceInNumber(123, 1);
            var result2 = programLoop.CountDigitApperanceInNumber(123, 0);
            var result3 = programLoop.CountDigitApperanceInNumber(12, 3);
            var result4 = programLoop.CountDigitApperanceInNumber(333, 3);
            var result5 = programLoop.CountDigitApperanceInNumber(12377, 7);
            Assert.AreEqual(1, result1);
            Assert.AreEqual(0, result2);
            Assert.AreEqual(0, result3);
            Assert.AreEqual(3, result4);
            Assert.AreEqual(2, result5);
        }

        [TestMethod()]
        public void CountMaxApperanceTest()
        {
            var programLoopMock = new Mock<ProgramLoop>
            {
                CallBase = true
            };
            programLoopMock.Setup(x => x.CountDigitApperanceInNumber(101, 1)).Returns(2);
            programLoopMock.Setup(x => x.CountDigitApperanceInNumber(101, 0)).Returns(1);
            programLoopMock.Setup(x => x.CountDigitApperanceInNumber(20, 0)).Returns(1);
            programLoopMock.Setup(x => x.CountDigitApperanceInNumber(20, 2)).Returns(1);
            programLoopMock.Setup(x => x.CountDigitApperanceInNumber(21, 2)).Returns(1);
            programLoopMock.Setup(x => x.CountDigitApperanceInNumber(21, 1)).Returns(1);
            programLoopMock.Setup(x => x.CountDigitApperanceInNumber(3, 3)).Returns(1);
            programLoopMock.Setup(x => x.CountDigitApperanceInNumber(111, 1)).Returns(3);
            programLoopMock.Setup(x => x.CountDigitApperanceInNumber(222, 2)).Returns(3);
            programLoopMock.Setup(x => x.CountDigitApperanceInNumber(5, 5)).Returns(1);
            var result1 = programLoopMock.Object.CountMaxApperance(new int[] { 101, 20, 21, 3 });
            var result2 = programLoopMock.Object.CountMaxApperance(new int[] { 111, 222, 5 });
            Assert.AreEqual(1, result1);
            Assert.AreEqual(2, result2);
        }
    }
}