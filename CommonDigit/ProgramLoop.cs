﻿using System;
using System.Linq;

namespace CommonDigit
{
    public class ProgramLoop
    {
        public void Run()
        {
            var numbers = new int[Int32.Parse(Console.ReadLine())];
            numbers = Console.ReadLine().Split(' ').Select(x => Int32.Parse(x)).ToArray();
            Console.WriteLine(CountMaxApperance(numbers));
            Console.ReadKey();
        }

        public virtual int CountDigitApperanceInNumber(int number, int digit)
        {
            int count = 0;
            while (number > 0)
            {
                if (number % 10 == digit)
                    count++;
                number /= 10;
            }
            return count;
        }

        public int CountMaxApperance(int[] numbers)
        {
            int maxApperance = 1;
            int result = 0;

            for(var i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] < 0)
                {
                    numbers[i] = -numbers[i];
                }
            }

            for (int d = 0; d <= 9; d++)
            {
                int apperanceOfDigit = 0;
                for (var i = 0; i < numbers.Length; i++)
                {
                    apperanceOfDigit += CountDigitApperanceInNumber(numbers[i], d);
                }
                if (apperanceOfDigit >= maxApperance)
                {
                    maxApperance = apperanceOfDigit;
                    result = d;
                }
            }
            return result;
        }
    }
}
